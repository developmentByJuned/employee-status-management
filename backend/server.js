const express = require("express");
const cors=require("cors");
const bodyParser = require("body-parser");
const PORT = 3001;
const app = express();
const corsOptions ={
   origin:'*', 
   credentials:true,
   optionSuccessStatus:200,
}

app.use(cors(corsOptions)) 
app.use(bodyParser.json());

const employees = [
  {
    id: 1,
    name: "John",
    status: "Working",
    img: "http://placekitten.com/200/300",
  },
  {
    id: 2,
    name: "Jack",
    status: "Working",
    img: "http://placekitten.com/200/300",
  },
  {
    id: 3,
    name: "Sheli",
    status: "Working",
    img: "http://placekitten.com/200/300",
  },
  {
    id: 4,
    name: "Eitan",
    status: "Working",
    img: "http://placekitten.com/200/300",
  },
];

app.get("/users", (_, res) => {
  res.send(employees);
});

app.put("/users", (req, res) => {
  try {
    employees.push({
      ...req.body,
      id: employees.length + 1,
      img: "http://placekitten.com/200/300",
    });
    res.send(employees);
  } catch (err) {
    console.log(err);
    res.status(500).json({ message: err.message });
  }
});

app.post("/users/:id", (req, res) => {
  try {
    const index = employees.findIndex((obj) => obj.id === +req.params.id);
    employees[index].status = req.body.status;
    res.send(employees);
  } catch (err) {
    console.log(err);
    res.status(500).json({ message: err.message });
  }
});

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});

