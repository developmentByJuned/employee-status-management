export const API_ENDPOINT = "http://localhost:3001/";

export const STATUS = {
  Working: "Working",
  OnVacation: "On Vacation",
  LunchTime: "Lunch Time",
  BusinessTrip: "Business Trip",
};
