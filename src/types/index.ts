export type User = {
  id: number;
  name: string;
  status: string;
  img: string;
};

export type Status = {
  Working: string;
  OnVacation: string;
  LunchTime: string;
  BusinessTrip: string;
};
