import React from "react";
import EmployeeListing from "../components/EmployeeListing";
import { API_ENDPOINT } from "../constant";
import { Status, User } from "../types";
import { Button } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import AddEmployeeModal from "../components/AddEmployeeModal";
const Dashboard = () => {
  const [employeeList, setEmployeeList] = React.useState<User[]>([]);
  const [showModal, setShowModal] = React.useState<boolean>(false);

  React.useEffect(() => {
    fetchEmployeeList();
  }, []);

  const fetchEmployeeList = () => {
    fetch(API_ENDPOINT + "users")
      .then((res) => res.json())
      .then((res) => {
        setEmployeeList(res);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleStatusChange = (id: number, status: string) => {
    fetch(API_ENDPOINT + "users/" + id, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      redirect: "follow",
      body: JSON.stringify({ status }),
    })
      .then((res) => res.json())
      .then((res) => {
        setEmployeeList(res)
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const toggleModal = () => setShowModal(!showModal);

  const handleModalSubmit = (value: {name: string, status: string}) => {
    fetch(API_ENDPOINT + "users/", {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        redirect: "follow",
        body: JSON.stringify({ ...value }),
      })
        .then((res) => res.json())
        .then((res) => {
          toggleModal()
          setEmployeeList(res)
        })
        .catch((error) => {
          console.log(error);
        });
  }

  return (
    <div className="emp-list-container">
      <Button
        color="info"
        variant="contained"
        sx={{ fontSize: "1.1rem", height: "3.5rem", width: "10rem" }}
        onClick={toggleModal}
      >
        Create
        <AddIcon
          sx={{
            color: "#ffff",
            height: "1.6rem",
            width: "1.6rem",
            marginBottom: "2px",
          }}
        />
      </Button>
      <EmployeeListing
        employeeList={employeeList}
        handleStatusChange={handleStatusChange}
      />
      {showModal && (
      <AddEmployeeModal handleSubmit={handleModalSubmit} title="Create New User" open={showModal} handleClose={toggleModal} />
      )}
    </div>
  );
};

export default Dashboard;
