import * as React from "react";
import {
  Card,
  Typography,
  CardMedia,
  CardContent,
  Box,
  FormControl,
  NativeSelect,
} from "@mui/material";
import { User } from "../types";
import { STATUS } from "../constant";
interface EmpCardProps {
  employeeData: User;
  handleStatusChange: (id: number, status: string) => void;
}

const EmployeeCard: React.FC<EmpCardProps> = ({
  employeeData,
  handleStatusChange,
}): React.ReactElement => {
  return (
    <Box sx={{ display: "flex" }}>
      <Card className={`employee-card ${employeeData.status.toLowerCase()}`} sx={{ minWidth: 275, width: "25rem" }} variant="outlined">
        <Box sx={{ display: "flex", flexDirection: "row" }}>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              width: "50%",
              minHeight: "12rem",
            }}
          >
            <CardMedia
              component="img"
              sx={{ width: "70%", height: "70%", borderRadius: "50%" }}
              image={employeeData?.img}
              alt="Live from space album cover"
            />
          </Box>
          <CardContent
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "flex-start",
              justifyContent: "flex-end",
              textAlign: "left",
              marginBottom: "5%",
              width: "50%",
            }}
          >
            <Typography component="div" variant="h6">
              {employeeData?.name}
            </Typography>
            <Box sx={{display:'flex', gap: 1.5, width: '100%', alignItems: 'center', justifyContent: 'space-between'}}>
              <div className={`dot ${employeeData.status.toLowerCase()}`}></div>
              <FormControl fullWidth>
                <NativeSelect
                  value={employeeData.status}
                  inputProps={{
                    name: "emp-status",
                    id: "uncontrolled-native",
                  }}
                  onChange={(event) =>
                    handleStatusChange(employeeData.id, event.target.value)
                  }
                >
                  {Object.keys(STATUS).map((value, key) => (
                    <option key={key} value={value}>
                      {value}
                    </option>
                  ))}
                </NativeSelect>
              </FormControl>
            </Box>
          </CardContent>
        </Box>
      </Card>
    </Box>
  );
};

export default EmployeeCard;
