import React from "react";
import { User } from "../types";
import EmployeeCard from "./EmployeeCard";

interface EmployeeListProps {
  employeeList: User[];
  handleStatusChange: (id: number, status: string) => void;
}

const EmployeeListing: React.FC<EmployeeListProps> = ({
  employeeList,
  handleStatusChange
}): React.ReactElement => {
  return employeeList && employeeList.length ? 
  <div className="emp-card-listing">
  {employeeList.map((employeeData, idx) => (
    <EmployeeCard key={`employee-card-${idx}`} employeeData={employeeData} handleStatusChange={handleStatusChange} /> 
  ))}
  </div>
  :
   <></>;
};

export default EmployeeListing;
