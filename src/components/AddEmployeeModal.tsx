import React from "react";
import {
  Box,
  FormControl,
  Modal,
  NativeSelect,
  TextField,
  Typography,
  InputLabel,
  Button,
} from "@mui/material";
import { STATUS } from "../constant";
import { Status } from "../types";

interface EmpModalProps {
  open: boolean;
  handleClose: () => void;
  title: string;
  handleSubmit: (value: { name: string; status: string }) => void;
}
const style = {
  display: "grid",
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
  gap: 4,
};

const AddEmployeeModal: React.FC<EmpModalProps> = ({
  open,
  handleClose,
  title,
  handleSubmit,
}): React.ReactElement => {
  const [name, setName] = React.useState<string>("");
  const [statusVal, setStatusVal] = React.useState<string>(STATUS.Working);
  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <Typography
          id="modal-modal-title"
          fontWeight={600}
          variant="h6"
          component="h2"
        >
          {title}
        </Typography>
        <TextField
          label="User name:"
          defaultValue="Hello World"
          variant="standard"
          fullWidth
          value={name}
          onChange={(event: React.ChangeEvent<HTMLTextAreaElement>): void => {
            setName(event?.target.value);
          }}
        />
        <FormControl fullWidth>
          <InputLabel variant="standard" htmlFor="uncontrolled-native">
            Status:
          </InputLabel>
          <NativeSelect
            defaultValue={STATUS.Working}
            inputProps={{
              name: "emp-status",
              id: "uncontrolled-native",
            }}
            value={statusVal}
            onChange={(event: React.ChangeEvent<HTMLSelectElement>) => {
              const status = event.target.value;
              setStatusVal(status);
            }}
          >
            {Object.keys(STATUS).map((value: string, key: number) => (
              <option key={key} value={value}>
                {value}
              </option>
            ))}
          </NativeSelect>
        </FormControl>
        <Box sx={{ display: "flex", gap: 2 }}>
          <Button
            onClick={() => handleSubmit({ name, status: statusVal })}
            color="info"
            variant="contained"
            disableElevation
          >
            Create
          </Button>
          <Button onClick={handleClose} variant="text" className="emp-button">
            Cancel
          </Button>
        </Box>
      </Box>
    </Modal>
  );
};

export default AddEmployeeModal;
