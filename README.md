## Steps to run the web app.

Pre-requisites:
* Node

# Backend
* cd backend (** from the root directory goto backend folder**)
* npm i
* npm start

# Frontend
* yarn (** from the root directory **)
* yarn start

Congrats ! You have successfully run the app. Happy Coding :)